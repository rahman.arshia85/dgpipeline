from django.apps import AppConfig


class DinnergenieConfig(AppConfig):
    name = 'dinnerGenie'
