from django.urls import path
from django.urls import include
from . import views

urlpatterns = [
    path('', views.hello_world, name='hello_world'),
]
